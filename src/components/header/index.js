import styled from "styled-components";

export const Body = styled.div`
  font-family: "Cairo", sans-serif;
  width: 40%;

  @media (max-width: 800px) {
    width: 98%;
  }
`;

export const StyledControl = styled.div`
  padding: 10px;
  display: flex;
  justify-content: space-between;

  @media (max-width: 800px) {
    font-size: 1.6rem;
  }
`;

export const ButtonBox = styled.button`
  border: 1px solid white;
  border-radius: 4px;
  background: darkblue;
  font-size: 1.2rem;
  padding: 10px 25px;
  margin-top: 40px;
  cursor: pointer;

  @media (max-width: 800px) {
    margin-top: 20px;
    font-size: 1.6rem;
  }
`;
