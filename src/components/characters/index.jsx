import React from "react";
import { Card } from "antd";
import { motion } from "framer-motion";
import styled from "styled-components";

const Characters = ({
  characters,
  type = "",
  header,
  filter = "all",
  onSelect = () => {},
}) => {
  return (
    <StyledCharacter initial={{ scale: 0.1 }} animate={{ scale: 1.0 }}>
      <StyledHeader>{header}</StyledHeader>
      <StyledList>
        {characters
          .map(({ name, image, tipo }, key) => (
            <StyledCard
              key={key}
              hoverable
              type={tipo}
              onClick={() => onSelect({ name, image }, type)}
              style={{ width: 240 }}
              cover={<img alt="example" src={image} />}
            >
              <Card.Meta title={name} />
            </StyledCard>
          ))
          .filter((char) => {
            return char.props.type === filter || filter === "all";
          })}
      </StyledList>
    </StyledCharacter>
  );
};

export default Characters;

const StyledCard = styled(Card)`
  font-family: "Cairo", sans-serif;
  font-weight: 700;
  margin: 10px;
`;

const StyledHeader = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

const StyledCharacter = styled(motion.div)`
  display: flex;
  padding: 20px;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const StyledList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;
