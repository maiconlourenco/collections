import React from "react";
import { Routes, TopBar } from "./pages/imports";
import styled from "styled-components";
import "./App.css";

function App() {
  return (
    <div className="App">
      <TopBar />

      <Body>
        <Routes />
      </Body>
    </div>
  );
}

export default App;

const Body = styled.div`
  margin-top: 38px;
`;
