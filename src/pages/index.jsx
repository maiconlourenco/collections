import React from "react";
import { Home, RickAndMorty, Pokemon, Chart } from "./imports";
import { Switch, Route } from "react-router-dom";
import { useSelector } from "react-redux";

const Routes = () => {
  const switcher = useSelector((state) => state.switcher.page);

  return (
    <Switch>
      <Route path="/pie">
        <Chart />
      </Route>

      <Route path="/characters/:page">
        {switcher === "rick-and-morty" ? <RickAndMorty /> : <Pokemon />}
      </Route>

      <Route path="/">
        <Home />
      </Route>
    </Switch>
  );
};

export default Routes;
