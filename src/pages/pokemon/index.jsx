import React, { useEffect, useState } from "react";
import { Link, useParams, useHistory } from "react-router-dom";
import { Characters, Body, StyledControl, ButtonBox } from "../../components";
import { addCharacter, switchPokemon } from "../../redux/actions";
import { useDispatch, useSelector } from "react-redux";

const Pokemon = () => {
  const { page } = useParams();
  const history = useHistory();
  const [charactersAPI, setCharactersAPI] = useState([]);
  const collection = useSelector((state) => state.collection);
  const dispatch = useDispatch();

  useEffect(() => {
    fetch(`https://pokeapi.co/api/v2/pokemon?offset=${page}&limit=20`)
      .then((res) => res.json())
      .then(({ results }) => {
        setCharactersAPI(
          [...results].map(({ name, url }) => {
            const brokenUrl = url.split("/");
            const id = brokenUrl[brokenUrl.length - 2];
            return {
              name: name,
              image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
            };
          }) || []
        );
      });
  }, [history, page, setCharactersAPI]);

  return (
    <Characters
      onSelect={(character, tipo) => dispatch(addCharacter(character, tipo))}
      characters={charactersAPI}
      collection={collection}
      type={"pokemon"}
      header={
        <Body>
          <ButtonBox onClick={() => dispatch(switchPokemon(history))}>
            Mostrar Rick and Morty
          </ButtonBox>
          <StyledControl>
            <Link to={`/characters/${page > 19 ? page - 20 : 0}`}>
              {"<\\"}Anterior
            </Link>
            {page}
            <Link to={`/characters/${page < 130 ? parseInt(page) + 20 : 130}`}>
              Próximo{"/>"}
            </Link>
          </StyledControl>
        </Body>
      }
    />
  );
};

export default Pokemon;
