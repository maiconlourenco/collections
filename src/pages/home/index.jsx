import React, { useState } from "react";
import { Characters } from "../../components";
import styled from "styled-components";
import { Select } from "antd";

import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { delCharacter } from "../../redux/actions";

const { Option } = Select;

const Home = () => {
  const [filter, setFilter] = useState("all");
  const collection = useSelector((state) => state.characters.collection);

  const dispatch = useDispatch();

  const onChange = (value) => {
    setFilter(value);
  };

  return (
    <Characters
      onSelect={(character, tipo) => dispatch(delCharacter(character))}
      filter={filter}
      header={
        <Filter>
          <Title>My Collection</Title>
          <Select
            defaultValue="all"
            style={{ width: 200 }}
            optionFilterProp="children"
            onChange={onChange}
          >
            <Option value="all">All</Option>
            <Option value="rick-and-morty">Rick and Morty</Option>
            <Option value="pokemon">Pokemon</Option>
          </Select>
          <Line />
        </Filter>
      }
      characters={collection}
      collection={collection}
    />
  );
};

export default Home;

const Filter = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Title = styled.h2`
  font-size: 3rem;
  font-family: "Cairo", sans-serif;
  font-weight: 700;
  color: white;
`;

const Line = styled.div`
  border-top: 3px solid powderblue;
  width: 70vw;
  margin 20px 0;
`;
