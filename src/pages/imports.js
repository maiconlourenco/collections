import Home from "./home";
import RickAndMorty from "./rick-and-morty";
import Pokemon from "./pokemon";
import Chart from "./chart";
import Routes from "./index";
import TopBar from "./top-bar";
export { Home, RickAndMorty, Pokemon, Chart, Routes, TopBar };
