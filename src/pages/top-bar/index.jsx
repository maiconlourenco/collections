import React from "react";
import { Link } from "react-router-dom";
import { MdCollections } from "react-icons/md";
import { BsFillPeopleFill } from "react-icons/bs";
import { AiFillPieChart } from "react-icons/ai";

import styled from "styled-components";
import { motion } from "framer-motion";

const TopBar = () => {
  return (
    <TopBarStyle>
      <TopBarLinks>
        <motion.div whileHover={{ scale: 1.3 }} whileTap={{ scale: 0.8 }}>
          <StyledLink to="/">
            <MdCollections />
          </StyledLink>
        </motion.div>

        <motion.div whileHover={{ scale: 1.3 }} whileTap={{ scale: 0.8 }}>
          <StyledLink to="/characters/1">
            <BsFillPeopleFill />
          </StyledLink>
        </motion.div>

        <motion.div whileHover={{ scale: 1.3 }} whileTap={{ scale: 0.8 }}>
          <StyledLink to="/pie">
            <AiFillPieChart />
          </StyledLink>
        </motion.div>
      </TopBarLinks>
    </TopBarStyle>
  );
};

export default TopBar;

const TopBarLinks = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

const TopBarStyle = styled.div`
  background-color: white;
  width: 100%;
  position: fixed;
  top: 0;
  padding: 10px 5px;
  z-index: 10;
`;

const StyledLink = styled(Link)`
  margin: 30px;
`;
