import React from "react";
import { Pie } from "react-chartjs-2";
import styled from "styled-components";
import { useSelector } from "react-redux";

const Chart = () => {
  const collection = useSelector((state) => state.characters.collection);
  console.log(collection);
  const charactersData = collection.reduce((current, { tipo }) => {
    current[tipo] ? (current[tipo] += 1) : (current[tipo] = 1);
    return current;
  }, {});

  const data = {
    labels: Object.keys(charactersData),
    datasets: [
      {
        data: Object.values(charactersData),
        backgroundColor: ["#FF6384", "#36A2EB"],
        hoverBackgroundColor: ["#FF6384", "#36A2EB"],
      },
    ],
  };
  return (
    <PieBox>
      <Title>Gráfico</Title>
      <Line />
      <Pie
        data={data}
        legend={{
          labels: {
            fontColor: "rgb(255, 99, 132)",
            fontSize: 20,
          },
        }}
      />
    </PieBox>
  );
};

export default Chart;

const PieBox = styled.div`
  height: 400px;
  width: 800px;
  margin-bottom: 20vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media (max-width: 800px) {
    height: 150px;
    width: 300px;
    margin-bottom: 10vh;
  }
`;

const Title = styled.h2`
  font-size: 3rem;
  font-family: "Cairo", sans-serif;
  font-weight: 700;
  color: white;
`;

const Line = styled.div`
  border-top: 3px solid powderblue;
  width: 70vw;
  margin-bottom: 20px;
`;
