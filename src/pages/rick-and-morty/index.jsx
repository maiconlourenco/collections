import React, { useEffect, useState } from "react";
import { Link, useParams, useHistory } from "react-router-dom";
import { Characters, Body, StyledControl, ButtonBox } from "../../components";
import { addCharacter, switchRickAndMorty } from "../../redux/actions";
import { useDispatch, useSelector } from "react-redux";

const RickAndMorty = () => {
  const { page } = useParams();
  const history = useHistory();
  const [charactersAPI, setCharactersAPI] = useState([]);
  const dispatch = useDispatch();

  const collection = useSelector((state) => state.characters.collection);
  useEffect(() => {
    fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
      .then((res) => res.json())
      .then(({ results }) => setCharactersAPI(results || []));
  }, [history, page, setCharactersAPI]);

  return (
    <Characters
      onSelect={(character, tipo) => dispatch(addCharacter(character, tipo))}
      characters={charactersAPI}
      collection={collection}
      type={"rick-and-morty"}
      header={
        <Body>
          <ButtonBox onClick={() => dispatch(switchRickAndMorty(history))}>
            Mostrar Pokemon
          </ButtonBox>
          <StyledControl>
            <Link to={`/characters/${page > 1 ? page - 1 : 1}`}>
              {"<\\"} Anterior
            </Link>
            {page}
            <Link to={`/characters/${page < 34 ? parseInt(page) + 1 : 34}`}>
              Próximo {"/>"}
            </Link>
          </StyledControl>
        </Body>
      }
    />
  );
};

export default RickAndMorty;
