import { ADD, DELETE, RICKANDMORTY, POKEMON } from "./types";

export const addCharacter = (character, tipo) => {
  return { type: ADD, character, tipo };
};

export const delCharacter = (character) => {
  return { type: DELETE, character };
};

export const switchRickAndMorty = (page = "") => {
  return { type: RICKANDMORTY, page };
};

export const switchPokemon = (page = "") => {
  return { type: POKEMON, page };
};
