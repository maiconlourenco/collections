import { ADD, DELETE } from "../actions/types";

import { notification } from "antd";

const defaultState = {
  collection: JSON.parse(localStorage.getItem("collection")) || [],
};

const characters = (state = defaultState, action) => {
  switch (action.type) {
    case ADD:
      const alreadyAdd = state.collection.some(
        ({ name }) => name === action.character.name
      );

      if (alreadyAdd) {
        notification.error({
          key: action.character.name,
          message: "Erro",
          description: `${action.character.name} já foi adicionado!`,
        });
        return state;
      } else {
        notification.success({
          key: action.character.name,
          message: "Boa!",
          description: `${action.character.name} adicionado!`,
        });
        localStorage.setItem(
          "collection",
          JSON.stringify([
            ...state.collection,
            { ...action.character, tipo: action.tipo },
          ])
        );
        return {
          collection: [
            ...state.collection,
            { ...action.character, tipo: action.tipo },
          ],
        };
      }

    case DELETE:
      notification.info({
        key: action.character.name,
        message: "Boa!",
        description: `${action.character.name} removido!`,
      });
      const charIndex = state.collection.findIndex(
        (currentValue, index) => currentValue.name === action.character.name
      );

      state.collection.splice(charIndex, 1);
      localStorage.setItem("collection", JSON.stringify([...state.collection]));
      return { collection: [...state.collection] };

    default:
      return state;
  }
};

export default characters;
