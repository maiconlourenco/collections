import { combineReducers } from "redux";

import characters from "./characters";
import switcher from "./switcher";

export default combineReducers({ characters, switcher });
