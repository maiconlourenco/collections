import { RICKANDMORTY, POKEMON } from "../actions/types";

const defaultState = {
  page: "rick-and-morty",
};

const switcher = (state = defaultState, action) => {
  switch (action.type) {
    case RICKANDMORTY:
      action.page.push("0");
      return { page: "pokemon" };

    case POKEMON:
      action.page.push("1");
      return { page: "rick-and-morty" };

    default:
      return state;
  }
};

export default switcher;
